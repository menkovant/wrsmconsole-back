package musicutil

import (
	"github.com/graphql-go/graphql"
	"github.com/ungerik/go-dry"
	"log"
	"model"
	"storage"
)

var (
	discType    *graphql.Object
	MusicSchema graphql.Schema
)

type MusicUtil struct {
	dbConnection *storage.DbConnection
}

func Create(dbConnection *storage.DbConnection) *MusicUtil {
	log.Println("Musicutil Create")
	musicUtil := new(MusicUtil)
	musicUtil.dbConnection = dbConnection

	discType = graphql.NewObject(graphql.ObjectConfig{
		Name:        model.NAME,
		Description: "A set of songs from one or many artists.",
		Fields: graphql.Fields{
			model.ID: &graphql.Field{
				Type:        graphql.NewNonNull(graphql.String),
				Description: "The Identifier of the disc.",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if disc, ok := p.Source.(model.Disc); ok {
						return disc.Id, nil
					}
					return nil, nil
				},
			},
			model.TITLE: &graphql.Field{
				Type:        graphql.String,
				Description: "The Title of the album.",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if disc, ok := p.Source.(model.Disc); ok {
						return disc.Title, nil
					}
					return nil, nil
				},
			},
			model.ARTIST: &graphql.Field{
				Type:        graphql.String,
				Description: "The Artist of the album.",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if disc, ok := p.Source.(model.Disc); ok {
						return disc.Artist, nil
					}
					return nil, nil
				},
			},
			model.YEAR: &graphql.Field{
				Type:        graphql.String,
				Description: "The album release year.",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if disc, ok := p.Source.(model.Disc); ok {
						return disc.Year, nil
					}
					return nil, nil
				},
			},
		},
	})

	queryType := graphql.NewObject(graphql.ObjectConfig{
		Name: "Query",
		Fields: graphql.Fields{
			model.TABLE_NAME: &graphql.Field{
				Type: graphql.NewList(discType),
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					return musicUtil.GetAllDiscs(), nil
				},
			},
		},
	})

	createDiscType := graphql.NewInputObject(graphql.InputObjectConfig{
		Name: "CreateDisc",
		Fields: graphql.InputObjectConfigFieldMap{
			model.TITLE: &graphql.InputObjectFieldConfig{
				Type:        graphql.NewNonNull(graphql.String),
				Description: "The title of the disc.",
			},
			model.ARTIST: &graphql.InputObjectFieldConfig{
				Type:        graphql.NewNonNull(graphql.String),
				Description: "The artist of the disc.",
			},
			model.YEAR: &graphql.InputObjectFieldConfig{
				Type:        graphql.NewNonNull(graphql.String),
				Description: "The release year of the disc.",
			},
			model.ID: &graphql.InputObjectFieldConfig{
				Type:        graphql.NewNonNull(graphql.String),
				Description: "The id of the disc.",
			},
		},
	})

	mutationType := graphql.NewObject(graphql.ObjectConfig{
		Name: "MutationType",
		Fields: graphql.Fields{
			"createDiscMutation": &graphql.Field{
				Type: graphql.NewList(discType),
				Args: graphql.FieldConfigArgument{
					"input": &graphql.ArgumentConfig{
						Description: "An input with the disc details",
						Type:        graphql.NewNonNull(createDiscType),
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					var inp = p.Args["input"].(map[string]interface{})
					discToAdd := model.Disc{
						Title:  inp[model.TITLE].(string),
						Artist: inp[model.ARTIST].(string),
						Year:   inp[model.YEAR].(string),
						Id:     inp[model.ID].(string),
					}
					return musicUtil.AddDisc(discToAdd), nil
				},
			},
			"delete": &graphql.Field{
				Type:        graphql.NewList(discType),
				Description: "Delete disc by id",
				Args: graphql.FieldConfigArgument{
					model.ID: &graphql.ArgumentConfig{
						Type: graphql.NewNonNull(graphql.String),
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					id, _ := p.Args["id"].(string)

					return musicUtil.DeleteDisc(id), nil
				},
			},
		},
	})

	MusicSchema, _ = graphql.NewSchema(graphql.SchemaConfig{
		Query:    queryType,
		Mutation: mutationType,
	})
	return musicUtil
}

func (musicUtil *MusicUtil) GetAllDiscs() []model.Disc {
	discs, err := musicUtil.dbConnection.GetAllDiscs()
	if !dry.IsZero(err) {
		log.Println(err.Error())
		return nil
	}
	//	for _, disc := range discs {
	//		discs = append(discs, disc)
	//	}
	return discs
}

func (musicUtil *MusicUtil) AddDisc(newDisc model.Disc) []model.Disc {
	err := musicUtil.dbConnection.InsertDisc(newDisc)
	if !dry.IsZero(err) {
		log.Println(err.Error())
	}
	return musicUtil.GetAllDiscs()
}

func (musicUtil *MusicUtil) DeleteDisc(id string) []model.Disc {
	err := musicUtil.dbConnection.DeleteDisc(id)
	if !dry.IsZero(err) {
		log.Println(err.Error())
	}
	return musicUtil.GetAllDiscs()
}
