package model

const (
	NAME       = "disc"
	TABLE_NAME = "discs"
	ID         = "id"
	TITLE      = "title"
	ARTIST     = "artist"
	YEAR       = "year"
)

type Disc struct {
	Id     string `db:"id"`
	Title  string `db:"title"`
	Artist string `db:"artist"`
	Year   string `db:"year"`
}
