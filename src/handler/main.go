package main

import (
	"context"
	"encoding/json"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/graphql-go/graphql"
	"graph"
	"log"
	"storage"
)

func main() {
	lambda.Start(Handler)
}
func Handler(ctx context.Context, body json.RawMessage) (*graphql.Result, error) {
	log.Println("Handler received data")
	dbConnection, _ := storage.CreateConnection()
	defer dbConnection.CloseConnection()
	musicutil.Create(dbConnection)
	var apolloQuery map[string]interface{}
	if err := json.Unmarshal(body, &apolloQuery); err != nil {
		return nil, err
	}
	query := apolloQuery["query"]
	variables := apolloQuery["variables"]
	result := graphql.Do(graphql.Params{
		Schema:         musicutil.MusicSchema,
		RequestString:  query.(string),
		VariableValues: variables.(map[string]interface{}),
	})

	return result, nil
}
