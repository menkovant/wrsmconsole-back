package storage

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"log"
	"model"
)

type DbConnection struct {
	conn *sqlx.DB
}

var schema = "CREATE TABLE IF NOT EXISTS `" +
	model.TABLE_NAME + "` (`" +
	model.ID + "` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `" +
	model.TITLE + "` varchar(255) NOT NULL, `" +
	model.ARTIST + "` varchar(255) NOT NULL, `" +
	model.YEAR + "` varchar(255) NOT NULL)"

func CreateConnection() (dbConn *DbConnection, err error) {
	log.Println("CreateConnection")
	db, err := sqlx.Open("mysql", "menkovant:wasdwasd@tcp(wrsmconsoledb.cr6pimif3zsb.eu-central-1.rds.amazonaws.com:3306)/wrsmconsole_db")

	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	dbConn = new(DbConnection)
	dbConn.conn = db
	db.MustExec(schema)
	return dbConn, nil
}

func (dbConn *DbConnection) GetAllDiscs() ([]model.Disc, error) {
	log.Println("storage: GetAllDiscs")
	discs := []model.Disc{}
	err := dbConn.conn.Select(&discs, "select * from "+model.TABLE_NAME)
	if err != nil {
		return nil, err
	}
	return discs, nil
}

func (dbConn *DbConnection) InsertDisc(disc model.Disc) error {
	log.Println("storage: InsertDisc")
	_, err := dbConn.conn.NamedExec("INSERT INTO "+
		model.TABLE_NAME+" ("+
		model.TITLE+","+
		model.ARTIST+","+
		model.YEAR+") VALUES (:"+
		model.TITLE+",:"+
		model.ARTIST+",:"+
		model.YEAR+")", disc)
	if err != nil {
		return err
	}
	return nil
}

func (dbConn *DbConnection) DeleteDisc(id string) error {
	log.Println("storage: DeleteDisc")
	_, err := dbConn.conn.Exec("DELETE FROM "+
		model.TABLE_NAME+" where "+
		model.ID+"=?", id)
	if err != nil {
		return err
	}
	return nil
}

func (dbConn *DbConnection) CloseConnection() {
	dbConn.conn.Close()
}
