#!/bin/sh

docker run --rm --network="host" \
    -v "$PWD":/var/task lambci/lambda:go1.x src/handler/handler '{
                                                                   "operationName": "CreateDiscMutation",
                                                                   "variables": {
                                                                     "input": {
                                                                       "id": "1234",
                                                                       "title": "asdfasd",
                                                                       "artist": "bfdbvb",
                                                                       "year": "1234"
                                                                     }
                                                                   },
                                                                   "query": "mutation CreateDiscMutation($input: CreateDisc!) {\n  createDiscMutation(input: $input) {\n    title\n    artist\n    year\n    id\n    __typename\n  }\n}\n"
                                                                 }'