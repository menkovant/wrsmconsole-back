#!/bin/sh

docker run --rm \
    -v $(pwd):/go/src \
    -e GOPATH=/go/src:/go/src/vendor \
    lambci/lambda:build-go1.x \
    bash -c  "cd /go/src/src/handler && go build"

sudo chmod 777 -R $(pwd)
